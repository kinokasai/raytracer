﻿namespace Raytracer

type Point = Vector3
type Direction = Vector3

type Light =
    {
        getDirection : Point -> Direction;
        color : Color
    }

module DirectionalLight =

    // Type annotation is required here
    let make (dir : Direction, color : Color) =
        let n = -1.0 * dir |> Vector.normalize
        { getDirection = (fun _ -> n); color = color }

    let makeWhite (dir : Direction) = make (dir, Colors.White)

module PointLight =

    let make (pos : Point, color : Color) =
        let getDir p = pos - p |> Vector.normalize
        { getDirection = getDir; color = color }

    let makeWhite (pos : Point) = make (pos, Colors.White)