﻿namespace Raytracer

[<AutoOpen>]
module private ColorHelper =
    let bound x = max 0.0 <| x |> min 1.0


// We create a "class" for operator overloading

type Color = struct
    val R : float
    val G : float
    val B : float

     new (r,g,b) = { R = bound r; G = bound g; B = bound b }

    static member (*) (c1 : Color, c2 : Color) =  Color (c1.R * c2.R, c1.G * c2.G, c1.B * c2.B)
    static member (*) (s : float, c : Color) =  Color (s * c.R, s * c.G, s * c.B)
    static member (+) (c1 : Color, c2 : Color) =  Color (c1.R + c2.R, c1.G + c2.G, c1.B + c2.B)

    static member Zero =  Color (0.0, 0.0, 0.0)
    static member One =  Color (1.0, 1.0, 1.0)

    static member Average (a: Color) (b: Color) =
        let a = a * a
        let b = b * b
        let c = 0.5 * (a + b)
        Color (sqrt c.R, sqrt c.G, sqrt c.B)
end

module Colors =

    let DarkSlateGray = Color(47./255., 79./255., 79./255.) 
    let Black =  Color(0.0, 0.0, 0.0)
    let White =  Color(1.0, 1.0, 1.0)

    let Red =  Color(1.0, 0.0, 0.0)
    let Green =  Color(0.0, 1.0, 0.0)
    let Blue =  Color(0.0, 0.0, 1.0)
    let Yellow = Color(0.5, 0.5, 0.0)


type Material = {
    ka : float;
    kd : float;
    ks : float;
    beta : float;
    gamma : float;
    color : Color
}

module Materials =
    let std = {ka = 0.2; kd = 0.8; ks = 0.3; beta = 1.0; gamma = 1.0; color = Colors.Blue}
    let ofColor c = {std with color = c}
    let make ka kd ks beta gamma color = {ka = ka; kd = kd; ks = ks ; beta = beta; gamma = gamma; color = color}