﻿namespace Raytracer

module Bitmap =
    let toSysColor (c : Color) =
        let f x = 255.0 * min 1.0 (max 0.0 x) |> int
        System.Drawing.Color.FromArgb(255, f c.R, f c.G, f c.B)

    let drawImage (resX : int, resY : int) =
        let pixels = SampleScene.createImage (resX, resY)

        use bitmap = new System.Drawing.Bitmap(resX, resY)

        // Used for antialiasing
        let averageColors (colors : Color option list) =

            let average c1 c2 =
                match c1, c2 with
                | None, _ -> None
                | _, None -> None
                | Some c1, Some c2 -> Some (Color.Average c1 c2)
            
            colors |> List.reduce average

        let setPixel x y colors =
            let c = averageColors colors
            match c with
            | None -> ()
            | Some c -> bitmap.SetPixel(x, resY - 1 - y, toSysColor c)

        use dc = System.Drawing.Graphics.FromImage(bitmap)
        dc.Clear(System.Drawing.Color.DarkSlateGray)

        pixels |> Array2D.iteri setPixel

        bitmap.Save(@"out.bmp")