﻿namespace Raytracer

module Sphere =
    
    let private solveQuadEquation (a : float,  b : float, c : float) =
        let rad = b*b - 4.0*a*c
        let k0 = b / (-2.0*a)
        match rad with
        | Negative -> []
        | NearZero -> [ k0 ]
        | Positive -> let sqrtRad = (sqrt rad)/(2.0 * a)
                      [ k0 + sqrtRad; k0 - sqrtRad ]

    let make (m : Vector3, radius : float, material : Material) : SceneObj =
        let r2 = radius * radius
        let hitTest (ray : Ray) =
            let getPoint t = ray.start + t * ray.dir
            let getResult t = 
                let pos = getPoint t
                let normal = Vector.normalize (pos - m)
                { ray = ray; distance = t; pos = pos; normal = normal; material = material}
            let ts =
                let v0 = m - ray.start
                let b = -2.0*(v0<*>ray.dir)
                let c = (v0 <*> v0) - r2
                solveQuadEquation (1.0, b, c)
            match ts |> List.filter isPositive with
            | [t]    -> t |> getResult |> Some
            | [a; b] -> (min a b) |> getResult |> Some
            | _      -> None
        { hitTest = hitTest }