﻿namespace Raytracer

type HitResult =
    {
        ray : Ray;
        distance : float;
        pos : Point;
        normal : Direction;
        material : Material 
     }

type SceneObj = { hitTest : Ray -> HitResult option }

module RayTrace =

    let findHitObj objs (ray : Ray) =
        objs
        |> Seq.map (fun o -> o.hitTest ray)
        |> Seq.filter Option.isSome |> Seq.map Option.get
        |> Seq.sortBy (fun h -> h.distance)
        |> Seq.tryFind (fun _ -> true)

    let shade objs (hit : HitResult) (light : Light) : Color =
        let lightDir = hit.pos |> light.getDirection
        let rayToLight = Ray.make hit.pos lightDir
        let lightVisible = findHitObj objs rayToLight |> Option.isNone


        let ambient = hit.material.ka
        let diffuse = hit.material.kd
        let specular = hit.material.ks
        // helper - we are only interessted in positive values
        // get the fraction of diffuse Light (cap at 0)
        let diffF, specF =
            if lightVisible then
                let floorZero x = if x <= 0.0 then 0.0 else x
                let diffF = (hit.normal <*> lightDir) |> floorZero
                let reflDir = hit.ray.dir - 2.0 * (hit.normal <*> hit.ray.dir) * hit.normal;
                let specF = (reflDir <*> lightDir) |> floorZero
                diffF, specF
            else 
                0.0, 0.0
        let color = hit.material.color
        let diffuseColor = diffuse * diffF * color * light.color
        let specularColor = System.Math.Pow(specF, 10.0) * hit.material.beta * light.color
        let ambientColor = ambient * hit.material.color * light.color
        ambientColor + specularColor + diffuseColor

    let rec traceRay iter objs (lgs : Light seq) (ray : Ray) =

        if iter <= 0 then None else  // Ugly but necessary for readability
        let lgs = lgs |> Array.ofSeq

        let strMod (color : Color) = (1.0 / (float lgs.Length)) * color
        // search for a hitpoint
        let hit = findHitObj objs ray
        // get the shaded color if a point was hit
        let shadeHit (hitPoint) = (lgs |> Array.sumBy (strMod << shade objs hitPoint))

        // calculate reflected color
        let reflectHit (hitPoint : HitResult) =
            if hitPoint.material.gamma |> isPositive then
                let reflDir = hitPoint.ray.dir - 2.0 * (hitPoint.normal <*> hitPoint.ray.dir) * hitPoint.normal;
                let reflRay = Ray.make hitPoint.pos reflDir
                let reflRes = traceRay (iter - 1) objs lgs reflRay
                match reflRes with
                | None -> Colors.Black
                | Some x -> hitPoint.material.gamma * x
            else
                Colors.Black
        hit |> Option.map (fun hit -> shadeHit hit + reflectHit hit)
