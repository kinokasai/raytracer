﻿namespace Raytracer

type Ray = {
    start : Point;
    dir : Point;
}

type RayRaster = Ray list[,]

module Ray =

    let make s d = {start = s; dir = d}

    let fromPoints start point =
        let dir = Vector.normalize (point - start)
        make start dir