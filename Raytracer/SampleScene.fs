﻿namespace Raytracer

module SampleScene =

    let private scene = 
        let eye = Vector.make (0.0, 0.0, -5.0)
        let center = Vector.Zero
        let up = Vector.make (0.0, 1.0, 0.0)
        let right = Vector.make (1.0, 0.0, 0.0)
        let vp = ViewPort (eye, center, up, right, 8.3, 8.3)
        let s = Scene.empty vp

        let sphere = Sphere.make (Vector.make (0.0, 0.0, 40.0), 10.0, Materials.ofColor(Colors.Blue))
        let otherSphere = Sphere.make (Vector.make (15.0, -20.0, 35.0), 10.0, Materials.ofColor(Colors.Green))
        let thirdSphere = Sphere.make (Vector.make (-10.0, -20.0, 25.0), 5.0, Materials.ofColor(Colors.Red))
        let quadSphere = Sphere.make (Vector.make (12.0, 12.0, 33.0), 3.0, Materials.ofColor(Colors.Yellow))
        let fifthSphere = Sphere.make (Vector.make (-30.0, 12.0, 100.0), 10.0, Materials.ofColor(Colors.Red))
        let sun = PointLight.makeWhite(Vector.make (1.0, 10.0, -10.0))
        let dir = DirectionalLight.makeWhite(Vector.make (-10.0, -10.0, 1.0))

        Scene.empty vp |> Scene.addObj sphere
                       |> Scene.addObj otherSphere
                       |> Scene.addObj thirdSphere
                       |> Scene.addObj quadSphere
                    //    |> Scene.addObj fifthSphere
                    //    |> Scene.addLight sun
                       |> Scene.addLight dir

    let createImage (resX, resY) =
        Scene.trace (resX, resY) scene