﻿namespace Raytracer

[<AutoOpen>]
module private LazyHelpers =
    let createLen2 (x,y,z)              = lazy (x*x+y*y+z*z)
    let createLen  (len2 : Lazy<float>) = lazy ( len2.Force() |> sqrt )

[<AutoOpen>]
module FloatHelpers =

    let ZeroThreshold = 0.0000000001

    let isNearZero (f : float) =
        abs f <= ZeroThreshold

    let isPositive (f : float) =
        f > ZeroThreshold

    let isNegative (f : float) =
        f < -ZeroThreshold

    let (|NotNearZero|_|) (f : float) = 
        if isNearZero f
        then None
        else Some f

    let (|Negative|NearZero|Positive|) (f : float) =
        if f |> isNegative then Negative
        elif f |> isPositive then Positive
        else NearZero

// We create a class for Operator Overlading

type Vector3 = struct
    val X : float
    val Y : float
    val Z : float

    new (x,y,z) = { X = x; Y = y; Z = z }

    member v.Coords = [| v.X; v.Y; v.Z |]

    static member (+) (a : Vector3, b : Vector3) =  Vector3 (a.X + b.X, a.Y + b.Y, a.Z + b.Z)
    static member (-) (a : Vector3, b : Vector3) =  Vector3 (a.X - b.X, a.Y - b.Y, a.Z - b.Z)
    static member (*) (s : float,   v : Vector3) =  Vector3 (s * v.X, s * v.Y, s * v.Z)

    static member (<*>) (a : Vector3, b : Vector3) = a.X*b.X + a.Y*b.Y + a.Z*b.Z
end

module Vector =

    let make (x, y, z) = Vector3 (x, y, z)
    
    let Zero = make (0.0, 0.0, 0.0)

    let E1 = make (1.0, 0.0, 0.0)
    let E2 = make (0.0, 1.0, 0.0)
    let E3 = make (0.0, 0.0, 1.0)

    let sqr (v : Vector3) = v.X * v.X + v.Y * v.Y + v.Z * v.Z
    let length (v : Vector3) = v |> sqr |> sqrt

    let isZero (v : Vector3) =
        v.X |> isNearZero && v.Y |> isNearZero && v.Z |> isNearZero

    let normalize (v : Vector3) =
        match length v with
        | NotNearZero l -> (1.0 / l) * v
        | _             -> Zero

    let projectOnNormal (n : Vector3) (v : Vector3) = (n <*> v) * n

    let sameDirectionAsNormal (n : Vector3) (v : Vector3) =
        if v |> isZero then
            false
        else
            let cos = (n <*> v) / length v
            (1.0 - cos) |> isNearZero

    let sameDirection (a : Vector3) =
        if a |> isZero 
        then (fun _ -> false)
        else a |> normalize |> sameDirectionAsNormal