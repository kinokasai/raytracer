﻿namespace Raytracer

// A class is more simple to handle in our case 
type ViewPort(eye : Point, center : Point, dirUp : Direction, dirRight : Direction, height : float, width : float, ?antialiasParam : int) =
    
    let createRaster (resX : int, resY : int) : Point list[,] =
        let antialiasingLevel = defaultArg antialiasParam 1
        let dX = width / (float resX)
        let dY = height / (float resY)
        let start = center - ((width - dX) / 2.0) * dirRight - ((height - dY )/ 2.0) * dirUp

        Array2D.init resX resY
            (fun x y -> //[start + (float (x) * dX) * dirRight + (float y * dY) * dirUp]
                let x = float x
                let y = float y
                [
                start + ((x + 0.5) * dX) * dirRight + ((y + 0.5) * dY) * dirUp
                start + ((x + 0.5) * dX) * dirRight + (y * dY) * dirUp
                start + ((x + 0.5) * dX) * dirRight + ((y - 0.5) * dY) * dirUp
                start + ((x) * dX) * dirRight + ((y + 0.5) * dY) * dirUp
                start + ((x) * dX) * dirRight + (y * dY) * dirUp
                start + ((x) * dX) * dirRight + ((y - 0.5) * dY) * dirUp
                start + ((x - 0.5) * dX) * dirRight + ((y + 0.5) * dY) * dirUp
                start + ((x - 0.5) * dX) * dirRight + (y * dY) * dirUp
                start + ((x - 0.5) * dX) * dirRight + ((y - 0.5) * dY) * dirUp
                ]

            )

    let createRay (viewPortPoint : Point) =
        Ray.fromPoints eye viewPortPoint

    let createRays (viewPortPoint : Point list) =
        viewPortPoint |> List.map (Ray.fromPoints eye)

    member vp.CreateRaster (resX, resY) : RayRaster = 
        createRaster (resX, resY) |> Array2D.map createRays

module Scene =

    type Scene = {
        lights : Light list;
        objects : SceneObj list;
        viewPort : ViewPort;
    }

    let make l o v = {lights = l; objects = o; viewPort = v}

    let empty v = {lights = []; objects = []; viewPort = v}

    let addLight l s = {s with lights = l::s.lights}

    let addObj o s  = {s with objects = o::s.objects}

    let trace (resX, resY) scene =
        let lights = scene.lights
        let objects = scene.objects
        let viewPort = scene.viewPort
        
        let rays = viewPort.CreateRaster (resX, resY)
        let tracer = RayTrace.traceRay 3 objects lights

        let asyncTrace = ()

        let recompose (l : Color option list array) =
            let f = (fun x y -> l.[y + x*resX])
            Array2D.init resX resY f

        // We need to transform into a Seq for multithreading
        rays
        |> Seq.cast<Ray List>
        |> Seq.toArray
        |> Array.Parallel.map (fun rayl -> rayl |> List.map tracer)
        |> recompose
